#!/usr/bin/python3.6
import json
import sys

if sys.version_info[0] != 3 or sys.version_info[1] != 6:
    raise Exception("Python 3.6 is required.")
inventory = {
    '_meta': {
        'hostvars': {
            'servera': {
                'ip_address': '192.168.56.114',
                'os': 'Linux'
            },
            'serverb': {
                'ip_address': '192.168.56.115',
                'os': 'Linux'
            }
        }
    },
    'web_servers': {
        'hosts': ['servera', 'serverb'],
        'vars': {
            'web_server_var1': 'value1',
            'web_server_var2': 'value2'
        }
    }
}

print(json.dumps(inventory))
